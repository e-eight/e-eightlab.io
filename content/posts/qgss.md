+++
title = "Schrodinger's cat is alive"
date = 2020-08-03T00:00:00-05:00
tags = ["qc", "qiskit"]
draft = false
weight = 1
+++

> "Yet things might have gone far otherwise and far worse. When you think of the
> Battle of Pelennor, do not forget the battles in Dale and the valor of Durin's
> Folk. Think of what might have been. Dragon-fire and savage swords in Eriador,
> night in Rivendell. There might be no Queen in Gondor. We might now hope to
> return from the victory here to ruin and ash. But that has been averted —
> because I met Thorin Oakenshield one evening on the edge of spring in Bree. _A
> chance-meeting, as we say in Middle-earth._"
>
> --- Gandalf to Frodo and Gimli in The Lord of the Rings.


## Backstory {#backstory}

A chance-meeting is what led me to the Qiskit Global Summer School. And now I am
packing second breakfasts to go on an adventure like Bilbo. Physics, and in
particular quantum physics, has been my bread and butter for the last decade or
so, starting from my undergrad, and now in my final years as a graduate student.
All my research has been on quantum systems, but prior to the summer school, I
had never studied either quantum information or quantum computation. (Quantum
physics is a pretty huge field; here's a [map](https://www.youtube.com/watch?v=gAFAj3pzvAA) to help you navigate.) I was aware
of the existence of John Preskill's notes on quantum computation. I could
proudly recognize the picture of a Bloch Sphere in trivia/quiz competitions. I
had heard that Shor's algorithm can break the RSA encryption. And that was all I
knew about quantum information and computation till two weeks back. I think the
lack of access to a quantum computer or even a simulator, during my undergrad,
discouraged me from learning more about the subject. It was in this backdrop
that I had a chance-meeting, in very late June, with a good friend of mine from
undergrad, [Arun Ravishankar](https://medium.com/@arunravishankar).

At that time I was going through one of the leanest phases of my grad student
life. I could not see the big picture of my research, and was very unsure about
my post-graduation career plans. Add to that the stress that comes from being an
international student during a global pandemic. Will I have to go back without
completing my degree? Will there be a teaching assistant position for me in fall
if enrollment is low? What will the job market be like when I graduate in a
little over a year's time. I was lost, not sure where I was or where I could go.
And here comes my friend, with similar things on his mind. During our discussion
Arun mentioned that he had had a chat with someone from the quantum computation
industry, and was pondering if that was a possible career path for him. Given
our very similar tastes in physics, I figured this might be something that I
would like to do too. I started searching what sort of jobs were available in
that industry, what sort of research people were doing. I found only a handful
of company working on this topic, but the research was very interesting. The
catch was that I did not know any quantum computation. But I know quantum, and
some computation. How hard would it be to learn their intersection? I put on my
traveling salesman hat and started hopping from one quantum city to another,
trying to figure out which city would be the suitable starting point of this
journey. Turns out there are a lot of cities, but it just so happened that one
of these was the newly incorporated Qiskit Global Summer School, about to allow
visitors in a couple weeks' time. Aha! I had found the shortest route for my
traveling salesman problem. I filled in and submitted the application form with
haste, but careful not to make any mistakes, lest I not get selected. And now it
was time to wait. I got David Mermin's [Quantum Computer Science](http://www.cambridge.org/us/catalogue/catalogue.asp?isbn=9780521876582), and went
through [Coding with Qiskit](https://youtu.be/a1NZC5rqQD8) playlist as a sort of early prep.


## Meeting the party {#meeting-the-party}

Two days before the summer school started I got the link for the Discord
server, and I joined it. I was mildly surprised by the choice of Discord, as it
is known mainly as a gaming platform. (I had personally used it only to play
Dungeons and Dragons, prior to the summer school.) But it was quickly evident
why Discord was a right choice. The scale of the school was something I had
never seen before. Over 2000 students scattered across the world. The first
couple days on Discord was overwhelming for me, with new people joining every
few minutes, and new study groups popping up. But once things settled down a
bit, it turned out to be one of the best learning tools that I have experienced.
I believe we learn the most by discussing with our peers. The Discord server
made that reality in a pandemic-stricken world, and what more, not only could I
discuss with just 20 or 30 of my peers, but with all 2000 of them.

The first lecture was on the basics of quantum computation by Elisa Bäumer.
Teaching a class of 20 can be terrifying, but Elisa taught a class of 4000! And
she did it really well. Next we learned phase kickback, and the Deutsch-Josza
algorithm, my first quantum algorithm. The simplicity and the novelty of the
algorithm was just beautiful. Then came quantum Fourier transformation, quantum
phase estimation, and the poster-child of quantum algorithms, Shor's algorithm,
taught by fan-favorite Abe Asfaw. And to round of the week we had an excellent
lecture on quantum error correction by the Wrangler of qubits, James Wootton.
Along the way we also had lab exercises where we got to try out Qiskit, and
implement the quantum algorithms ourselves. I learned a lot by doing the labs.
Personally I would have preferred the labs to be more interactive, but given the
scale of the school it was difficult. Discord helped. A lot.

The second week started with, what was essentially a six-hour masterclass on
teaching physics by Zlatko Minev. Zlatko discussed everything from Newton's laws
to circuit quantum electrodynamics. The concepts were not new to me, but his
lectures helped me see how closely they were related to each other. These were
followed by lectures on quantum chemistry simulations by Antonio Mezzacapo and
Abhinav Kandala. For me the lectures by Antonio and Abhinav were probably of the
most practical importance. Mapping fermionic Hamiltonians to qubits, and using
quantum variational eigensolvers to get the ground state, is something that I
can readily use in my nuclear physics research.

The best was saved for the last day. The panel discussion on careers in quantum
computing was very illuminating. Quantum computing stalwarts Jay Gambetta, John
Preskill, Monika Schleier-Smith, Marina Radulaski, Steve Girvin, K. Birgitta
Whaley discussed their experience in the field and had some excellent advice for
budding quantum computing enthusiasts. I personally found these two comments by
John Preskill and Jay Gambetta to be the most helpful.

{{< figure src="/images/preskill_career_advice_twitter.png" >}}


## The adventure begins {#the-adventure-begins}

This summer school was hands-down one of the best learning experiences that I
have had. Got introduced to a completely new world, interacted with some amazing
mentors, and made some new connections. The question is where do I go from here?
I am still lost, but it is a good kind of lost. I now have a lantern, and
breadcrumbs on the ground, to show me my way. And where does this way take me? I
hope to the other side of the summer school. The plan is simple:

-   Learn more. Quantum computation is still a nascent field, and there are a lot
    of directions one can go. A broad understanding of the field will be very
    helpful.
-   Practice more. Qiskit has made it really simple to implement quantum circuits
    and algorithms. The learning process is greatly augmented by practicing. And
    practical limitations and ways to circumvent them will become apparent only if
    you actually implement what you have read.
-   Contribute to the community. This can be by teaching others (which brings
    clarity to your own understanding of the concepts), contributing to the
    codebase of open source quantum computing frameworks, like Qiskit. Or even by
    making Qiskit tutorials.
-   Network, and collaborate. Talk to the people, reach out to experts. This
    will help you to get a big picture of where the field is going, and how
    you can contribute. Humans are social beings, and being part of an active
    community is integral for effective learning.

I do not expect the adventure to be easy, but I think it will be a lot of fun,
like the best adventures are.


## Gears and supplies {#gears-and-supplies}

Every adventure needs the right set of tools; you probably do not want to use a
surfboard to help you rock climb. This adventure is no exception. And what's
better? Almost all that you need are available for free.

-   [Qiskit Textbook](https://qiskit.org/textbook/preface.html): Learn by doing.
-   [Qiskit Tutorials](https://github.com/Qiskit/qiskit-tutorials): More advanced tutorials. You can contribute too.
-   [Quantum computation course by Umesh Vazirani](https://people.eecs.berkeley.edu/~vazirani/f19quantum.html): You will find problem sets,
    project ideas, and reference materials.
-   [How to start your first quantum computing project](https://www.mustythoughts.com/how-to-start-your-first-quantum-computing-project): Precisely what the name
    says.
-   [Awesome Quantum Games](https://github.com/HuangJunye/Awesome-Quantum-Games): If games are your jam.
-   [Uncertain Systems](https://uncertain-systems.com/): If you wanna solve puzzles instead.
-   [IndiQ](https://indiq.org) and [Indi Quantum Community](https://disboard.org/server/734324671431966822): Quantum computing communities you can be a
    part of.
-   [Qiskit Advocates Program](https://qiskit.org/advocates/): Get your foot in the door.


## Acknowledgments {#acknowledgments}

Thanks to IBM for organizing this event. A bright spot in the otherwise
abysmal 2020. Thanks to the mentors and teachers, for those amazing lectures,
and answering our questions on Discord and Slack during and beyond the summer
school. Finally thanks to our hosts Brian Ingmanson, Suzie Mae, and Josie Kies
for making sure that everything ran smoothly, and Amira Abbas for moderating the
career panel discussion.
