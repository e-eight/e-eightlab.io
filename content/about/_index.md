+++
title = "About"
author = ["Soham Pal"]
date = 2021-02-12T00:00:00-06:00
draft = false
creator = "Emacs 27.1 (Org mode 9.5 + ox-hugo)"
weight = 2
+++

{{< figure src="/images/profile.jpg" width="50%" >}}

Hi! I am Soham Pal, a PhD candidate in theoretical nuclear physics at Iowa State
University. My research is on the application of chiral effective field theory
and machine learning to many body nuclear structure theory. You can find my
publications [here](https://scholar.google.com/citations?hl=en&view%5Fop=list%5Fworks&gmla=AJsN-F41Ct8VnJNiZxRpuL3aSfb9HD2BeoBMaC2WCOLHA4LnONZTh0DvdxIitmMr9TKb0cTUTYG2lA5ubAgtn10744AurqDC9xEjsXuBhja3QbWE5nSTYgA&user=04pQF6gAAAAJ).

I am also a Qiskit advocate at IBM, working on some quantum computing projects.

I will be using this space mostly to blog about my explorations in quantum
computing and machine learning.

Contact info:

-   Email: `soham@iastate.edu`
-   Twitter: `@dragonbornmonk`
-   LinkedIn: `/in/sohampal`
